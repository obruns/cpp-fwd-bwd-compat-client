#include <vendor.hpp>

#include <iostream>

auto to_be_implemented_by_the_client(const type_passed_to_the_client &input)
    -> void {
  std::cout << input.one << '\n';
}
