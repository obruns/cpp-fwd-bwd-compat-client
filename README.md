# Client code for the forward and backward compatibility demonstration

You probably want to clone this as a [submodule](https://git-scm.com/docs/git-submodule) as part of [this superproject](../cpp-forward-backward-compatibility).
